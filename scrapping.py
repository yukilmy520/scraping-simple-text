import requests
from bs4 import BeautifulSoup
import pandas as pd
import os

def get_countries_info():
    countries_info = 'https://www.scrapethissite.com/pages/simple/'
    response = requests.get(countries_info)
    if response.status_code != 200:
        raise Exception('Failed to load page {}'.format(countries_info))
    doc = BeautifulSoup(response.text,'html.parser')
    return doc

doc = get_countries_info()


## retrieve the name of countries 
def get_countries_name(doc):
    selection_class = 'country-name'
    countries_name_tags = doc.find_all('h3',{'class':selection_class})
    countries_names =[]
    for tag in countries_name_tags:
        countries_names.append(tag.text.strip())
    return countries_names

names = get_countries_name(doc)

## retrieve the Capital of countries 
def get_countries_capital(doc):
    selection_class = 'country-capital'
    countries_capital_tags = doc.find_all('span',{'class':selection_class})
    countries_capitals =[]
    for tag in countries_capital_tags:
        countries_capitals.append(tag.text.strip())
    return countries_capitals

capitals = get_countries_capital(doc)

def get_countries_population(doc):
    selection_class = 'country-population'
    countries_population_tags = doc.find_all('span',{'class':selection_class})
    countries_populations =[]
    for tag in countries_population_tags:
        countries_populations.append(int(tag.text))
    return countries_populations

populations = get_countries_population(doc)

## retrieve the areas of countries 
def get_countries_areas(doc):
    selection_class = 'country-area'
    countries_area_tags = doc.find_all('span',{'class':selection_class})
    countries_areas =[]
    for tag in countries_area_tags:
        countries_areas.append(float(tag.text))
    return countries_areas

areas = get_countries_areas(doc)


## PUT it all together and convert into dataframe
def scrape_countries():
    topics_url = 'https://www.scrapethissite.com/pages/simple/'
    response = requests.get(topics_url)
    if response.status_code != 200:
        raise Exception('Failed to load page {}'.format(topic_url))
    doc = BeautifulSoup(response.text, 'html.parser')
    topics_dict = {
        'Country Name': get_countries_name(doc),
        'Capital': get_countries_capital(doc),
        'Population': get_countries_population(doc),
        'Area': get_countries_areas(doc)
    }
    return pd.DataFrame(topics_dict)

data = scrape_countries()
print(data)
